var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

const mongoDB         =   'mongodb://localhost/testdb';
const baseURL         =   'http://localhost:3000/api/bicicletas';
const obj_bici_1      =   new Bicicleta({code: 10, color: 'rojo', modelo: 'urbana', ubicacion: [95.55,-84.12]});
const obj_bici_2      =   new Bicicleta({code: 30, color: 'amarillo', modelo: 'urbana', ubicacion: [95.57,-84.14]});
const str_bici        =   '{ "code": 30, "color": "azul", "modelo": "pistera", "lat": 95.58, "lng": -84.15 }';
const headers         =   {'Content-Type': 'application/json'};


describe('Bicicleta API', () => {
    beforeAll((done) => {
        mongoose.connection.close().then(() => {            
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true,useCreateIndex: true });
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('Conectados  a la base de datos de Prueba (testdb)');
                done();
            });
        });
    });

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);            
            done();
        });
    });

    
    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(baseURL, function(err, resp, body){
                var result = JSON.parse(body);
                expect(resp.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    
    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {      
            request.post({headers: headers, url: baseURL+'/create',body: str_bici}, function(err, resp, body) {
                expect(resp.statusCode).toBe(200);                               
                const newBici = JSON.parse(body).bicicleta;
                expect(newBici.color).toBe('azul');
                expect(newBici.modelo).toBe('pistera');
                expect(newBici.ubicacion[0]).toBe(95.58);
                expect(newBici.ubicacion[1]).toBe(-84.15);
                done();
            });
        });
    });

    describe('UPDATE Bicicletas /update', () => {
        it('Status 200', (done) => {       
            Bicicleta.add(obj_bici_2, (err, newBici)=>{request.post({headers: headers,url: baseURL+'/update',body: str_bici}, 
                function(err, resp, body) {
                    expect(resp.statusCode).toBe(200);
                    Bicicleta.findByCode(obj_bici_2.code, function (err, updatedBici) {                        
                        expect(updatedBici.color).toBe('azul');
                        expect(updatedBici.modelo).toBe('pistera');
                        expect(updatedBici.ubicacion[0]).toBe(95.58);
                        expect(updatedBici.ubicacion[1]).toBe(-84.15);
                        done();
                    });
                }); 
            });
        });
    });

    describe('DELETE Bicicletas /delete', () => {
        it('Status 204', (done) => {            
            Bicicleta.add(obj_bici_1, (err, newBici) => {              
                request.delete({headers: headers, url: baseURL+'/delete',body: '{"code" : 10}'
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(204);//Respuesta vacia                    
                    Bicicleta.allBicis(function (err, newBicis) {
                        expect(newBicis.length).toBe(0);
                        done();
                    });
                });
            });
        });
    });

});

 /*   
    describe('TIPOD DE CAMBIO',()=>{
    it('Prueba tipo de cambio',(done)=>{
        var hd = {'Content-Type': 'text/xml'};

        request.get({
            url:'https://gee.bccr.fi.cr/Indicadores/Suscripciones/WS/wsindicadoreseconomicos.asmx/ObtenerIndicadoresEconomicosXML?Indicador=318&FechaInicio=24/09/2020&FechaFinal=24/09/2020&Nombre=Juan Ali Ramirez Miranda&SubNiveles=N&CorreoElectronico=soporte@selvatura.com&Token=LRRUMEAA2E',
            headers:hd
        },(errr,response,body)=>{
            expect(response.statusCode).toBe(200);
            console.log(body);
            done();
        });

        })
    })
*/