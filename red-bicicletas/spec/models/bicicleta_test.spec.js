var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicleta',()=>{
    beforeAll((done) => { mongoose.connection.close(done) });
    
    beforeEach((done)=>{        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});        
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'MongoDB connection error: '));
        db.once('open',()=>{
            console.log('We are connected to test database');
            done();
        });
    });
    
    afterEach((done)=>{
        Bicicleta.deleteMany({},function(err,success){
            if(err) {console.log( err)}
            mongoose.connection.close(done);            
        });       	        
    });     
       
    
    describe('Bicicleta.createInstance',() => {
        it('Crear un ainstancia de bicicleta',() =>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[95.5,-84.12]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(95.5);
            expect(bici.ubicacion[1]).toEqual(-84.12);

        });
    });

    
    describe('Bicicleta.allBicis',() => {
        it('comienza vacia',(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici',(done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);                    
                    done();
                })
            })
        })
    });    

    describe('Bicicleta.findByCode',() => {
        it('Buscar la bicicleta con codigo 2', (done) => {

            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici,(err,newBici)=>{                    
                    if(err) console.log( err);

                    var aBici2 = new Bicicleta({code: 2, color: "lila", modelo: "montañera"});
                    Bicicleta.add(aBici2,(err,foundBici)=>{
                        if(err) console.log( err);

                        Bicicleta.findByCode(2,(err,foundBici)=>{
                            if(err) console.log( err);
                            expect(foundBici.code).toBe(aBici2.code);
                            expect(foundBici.color).toBe(aBici2.color);
                            expect(foundBici.modelo).toBe(aBici2.modelo);                            
                            done();
                        });

                    });
                });                 
            });
        });
    });

});





/*
describe('Bicicleta.allBicis',()=>{
    it('Coleccion comienza vacia', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    })
})

describe('Bicicletas.add',() => {
    var a = new Bicicleta(1,'rojo','urbana',[9.95, -84.11]);
    it('Agregamos una',() =>{        
        expect(Bicicleta.allBicis.length).toBe(0);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
        expect(Bicicleta.allBicis[0].color).toBe('rojo');
        expect(Bicicleta.allBicis[0].modelo).toBe('urbana');
    })
})

describe('Bicicleta.findByID',()=>{
    var a = new Bicicleta(1,'rojo','urbana',[9.95, -84.11]);
    var a2 = new Bicicleta(2,'amarillo','montyañera',[9.95, -84.11]);
    it('debe devolver una bici con id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        Bicicleta.add(a);
        Bicicleta.add(a2);
        var b = Bicicleta.findById(1);
        expect(a).toBe(a);
        expect(a.id).toBe(1);
        expect(a.color).toBe(b.color);
        expect(a.modelo).toBe(b.modelo);
    })
})

describe('Bicicleta.Bicicleta.removeById',()=>{
    var b1 = new Bicicleta(1,'rojo','urbana',[9.95, -84.11]);
    var b2 = new Bicicleta(2,'naranja','urbana',[9.95, -84.11]);
    var b3 = new Bicicleta(3,'verde','urbana',[9.95, -84.11]);
    
    it('Debe eliminar el elemento de id=2 de la coleccion',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        Bicicleta.add(b1);
        Bicicleta.add(b2);
        Bicicleta.add(b3);
        expect(Bicicleta.allBicis.length).toBe(3);
        Bicicleta.removeById(2);
        expect(Bicicleta.allBicis.length).toBe(2);
    } );

})

*/